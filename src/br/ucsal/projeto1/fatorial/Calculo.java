package br.ucsal.projeto1.fatorial;

public class Calculo {

	public int calcularFat(int num) {
		if (num <= 1)
			return 1;
		else
			return num * calcularFat(num - 1);
	}

	public boolean calcularPrimo(int numero) {

		for (int i = 2; i < numero; i++) {
			if (numero % i == 0) {
				return true;
			}
		}
		return false;
	}

}
