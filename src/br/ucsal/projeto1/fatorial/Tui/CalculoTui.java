package br.ucsal.projeto1.fatorial.Tui;

import java.util.Scanner;

import br.ucsal.projeto1.fatorial.Calculo;

public class CalculoTui {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Escreva: ");
		int valor = sc.nextInt();
		if (valor > 0) {
			Calculo c = new Calculo();
			System.out.println(c.calcularFat(valor));
			sc.close();
		} else {
			System.out.println("Valor Invalido");
			sc.close();
		}

	}
}
